import React from 'react';
import { useHistory } from 'react-router';
import s from './returnButton.module.scss';

export const ReturnButton = () => {
  const history = useHistory();
  return (
    <button type="button" className={s.returnButton} onClick={() => history.goBack()}>
      Back
    </button>
  );
};
