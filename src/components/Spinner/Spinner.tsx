import React from 'react';
import s from './spinner.module.scss';

export const Spinner = () => (
  <div className={s.loader}>Loading...</div>
);
