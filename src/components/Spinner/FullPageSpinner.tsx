import React from 'react';
import s from './spinner.module.scss';

export const FullPageSpinner = () => (
  <div className={s.spinnerWrapper}>
    <div className={s.loader}>Loading...</div>
  </div>
);
