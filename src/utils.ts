const CARDS_PER_PAGE = 20;

const capitalize = (s: string) => s.charAt(0).toUpperCase() + s.slice(1);

const createArrayInRange = (start: number, end: number) => {
  const result = [];
  for (let i = start; i <= end; i += 1) {
    result.push(i);
  }
  return result;
};

const getPokemonImageUrl = (id: number) => `https://pokeres.bastionbot.org/images/pokemon/${id}.png`;

const getPokemonListUrl = (limit: number, offset: number) => `https://pokeapi.co/api/v2/pokemon/?limit=${CARDS_PER_PAGE}&offset=${offset}`;

const getPokemonDataUrl = (id: number) => `https://pokeapi.co/api/v2/pokemon/${id}/`;

const getAbilityDataUrl = (id: number) => `https://pokeapi.co/api/v2/ability/${id}/`;

const getCardsPerPage = () => CARDS_PER_PAGE;

const extractIdFromUrl = (url: string) => {
  const extractingUrl = url.slice(0, url.length - 1);

  return parseInt(extractingUrl.slice(extractingUrl.lastIndexOf('/') + 1), 10);
};

export const utils = {
  capitalize,
  createArrayInRange,
  getPokemonImageUrl,
  getPokemonListUrl,
  getPokemonDataUrl,
  getAbilityDataUrl,
  getCardsPerPage,
  extractIdFromUrl,
};
