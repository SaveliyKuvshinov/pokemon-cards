import React from 'react';
import reactDom from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import { applyMiddleware, createStore } from 'redux';
import reduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { rootReducer } from './reducers';
import App from './App';

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(reduxThunk)));

reactDom.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);
