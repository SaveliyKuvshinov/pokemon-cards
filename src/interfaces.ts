export interface IPokemonAbility {
  ability: {name: string, url: string};
  isHidden: boolean;
  slot: number;
}

export interface IPokemonStat {
  base_stat: number; // eslint-disable-line
  effort: number;
  stat: {name: string, url: string};
}

export interface IPokemonType {
  slot: number;
  type: {name: string, url: string};
}

export interface IPokemon {
  id: number;
  name: string;
  types: IPokemonType[];
  abilities: IPokemonAbility[];
  stats: IPokemonStat[];
  image: string;
}

export interface IPokemonListEntry {
  id: number;
  url: string;
  name: string;
  image: string;
}

export interface IPokemonList {
  count: number;
  results: IPokemonListEntry[];
}

export interface IAbilityData {
  id: number;
  name: string;
  shortDescription: string;
  fullDescription: string;
}

export interface IPokemonState {
  abilityDataLoading: boolean;
  abilityDataErrors: Error[];
  abilityData: IAbilityData;
  pokemonDataLoading: boolean;
  pokemonDataErrors: Error[];
  pokemonData: IPokemon;
  pokemonListLoading: boolean;
  pokemonListErrors: Error[];
  pokemonList: IPokemonList;
}

export interface IPaginationState {
  pageCount: number;
  currentPage: number;
  offset: number;
}

export interface IRootState {
  paginationReducer: IPaginationState;
  pokemonReducer: IPokemonState;
}

export interface IAction {
  type: string;
  payload?: any;
}
