import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import s from './pagination.module.scss';
import { setCurrentPage, setOffset } from '../../../actions/paginationActions';
import { IRootState } from '../../../interfaces';
import { utils } from '../../../utils';

const PAGINATION_SIZE = 10;
const LEFT_ARROW = <span>&lsaquo;</span>;
const LEFT_DOUBLE_ARROW = <span>&laquo;</span>;
const RIGHT_ARROW = <span>&rsaquo;</span>;
const RIGHT_DOUBLE_ARROW = <span>&raquo;</span>;

export const Pagination = () => {
  const { pageCount, currentPage } = useSelector((state: IRootState) => state.paginationReducer);
  const dispatch = useDispatch();

  const onPageNumberClick = (pageNumber: number) => {
    dispatch(setCurrentPage(pageNumber));
    dispatch(setOffset((pageNumber - 1) * utils.getCardsPerPage()));
  };

  const onNextPageClick = () => {
    const newPageNumber = currentPage + 1;
    dispatch(setCurrentPage(newPageNumber));
    dispatch(setOffset((newPageNumber - 1) * utils.getCardsPerPage()));
  };

  const onPreviousPageClick = () => {
    const newPageNumber = currentPage - 1;
    dispatch(setCurrentPage(newPageNumber));
    dispatch(setOffset((newPageNumber - 1) * utils.getCardsPerPage()));
  };

  const onLastPageClick = () => {
    const newPageNumber = pageCount;
    dispatch(setCurrentPage(newPageNumber));
    dispatch(setOffset((newPageNumber - 1) * utils.getCardsPerPage()));
  };

  const onFirstPageClick = () => {
    const newPageNumber = 1;
    dispatch(setCurrentPage(newPageNumber));
    dispatch(setOffset((newPageNumber - 1) * utils.getCardsPerPage()));
  };

  const renderPages = () => {
    let endPage = PAGINATION_SIZE;
    let startPage = 1;

    if (endPage > pageCount) {
      endPage = pageCount;
    }

    const offset = Math.max(0, currentPage - (Math.floor(PAGINATION_SIZE / 2) + 1))
      - Math.max(0, (Math.floor(PAGINATION_SIZE / 2) - 1) - (pageCount - currentPage));

    endPage += offset;
    startPage += offset;

    return utils.createArrayInRange(startPage, endPage).map((pageNumber: number) => (
      <button
        type="button"
        className={currentPage === pageNumber ? s.paginationCellActive : s.paginationCell}
        onClick={() => onPageNumberClick(pageNumber)}
      >
        {pageNumber}
      </button>
    ));
  };

  return (
    <div className={s.wrapper}>
      <button
        onClick={onFirstPageClick}
        className={s.paginationCell}
        disabled={currentPage === 1}
        type="button"
      >
        {LEFT_DOUBLE_ARROW}
      </button>
      <button
        onClick={onPreviousPageClick}
        className={s.paginationCell}
        disabled={currentPage === 1}
        type="button"
      >
        {LEFT_ARROW}
      </button>
      {
        renderPages()
      }
      <button
        onClick={onNextPageClick}
        className={s.paginationCell}
        disabled={currentPage === pageCount}
        type="button"
      >
        {RIGHT_ARROW}
      </button>
      <button
        onClick={onLastPageClick}
        className={s.paginationCell}
        disabled={currentPage === pageCount}
        type="button"
      >
        {RIGHT_DOUBLE_ARROW}
      </button>
    </div>
  );
};
