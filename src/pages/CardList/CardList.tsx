import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import s from './cardList.module.scss';
import { Card } from './Card';
import { Pagination } from './Pagination';
import { getPokemonList } from '../../actions/pokemonActions';
import { IPokemonListEntry, IRootState } from '../../interfaces';
import { FullPageSpinner } from '../../components/Spinner';

export const CardList = () => {
  const { pokemonList, pokemonListLoading } = useSelector(
    (state: IRootState) => state.pokemonReducer,
  );
  const { offset } = useSelector((state: IRootState) => state.paginationReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getPokemonList(offset));
  }, [offset, dispatch]);

  return !pokemonListLoading && pokemonList.results
    ? (
      <div className={s.wrapper}>
        <div className={s.table}>
          {pokemonList.results.map((pokemon: IPokemonListEntry) => (
            <Card
              name={pokemon.name}
              image={pokemon.image}
              id={pokemon.id}
            />
          ))}
        </div>
        <Pagination />
      </div>
    )
    : (
      <FullPageSpinner />
    );
};
