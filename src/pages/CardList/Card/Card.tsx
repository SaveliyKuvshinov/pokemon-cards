import React, { useState } from 'react';
import { useHistory } from 'react-router';
import s from './card.module.scss';
import { Spinner } from '../../../components/Spinner';
import { utils } from '../../../utils';

interface ICardProps {
  id: number;
  image: string;
  name: string;
}

export const Card = ({ name, image, id }: ICardProps) => {
  const [loaded, setLoaded] = useState(false);
  const history = useHistory();

  const onCardClick = () => {
    history.push(`/details/${id}`);
  };

  return (
    <div tabIndex={0} role="button" onKeyPress={() => {}} className={s.wrapper} onClick={onCardClick}>
      <img
        className={s.image}
        src={image}
        alt="загрузка"
        style={loaded ? {} : { display: 'none' }}
        onLoad={() => setLoaded(true)}
      />
      {!loaded
            && (
            <div className={s.spinnerWrapper}>
              <Spinner />
            </div>
            )}
      <div className={s.details}>
        <span className={s.name}>{utils.capitalize(name)}</span>
      </div>
    </div>
  );
};
