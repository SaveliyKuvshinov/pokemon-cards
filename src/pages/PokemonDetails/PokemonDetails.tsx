import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import s from './pokemonDetails.module.scss';
import {
  IPokemonAbility, IPokemonStat, IPokemonType, IRootState,
} from '../../interfaces';
import { getPokemonData } from '../../actions/pokemonActions';
import { utils } from '../../utils';
import { ReturnButton } from '../../components/ReturnButton';
import { FullPageSpinner } from '../../components/Spinner';

export const PokemonDetails = () => {
  const { id }: { id: string } = useParams();
  const { pokemonDataLoading, pokemonData } = useSelector(
    (state: IRootState) => state.pokemonReducer,
  );
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(getPokemonData(parseInt(id, 10)));
  }, [dispatch, id]);

  const onAbilityClick = (abilityId: number) => {
    history.push(`/ability/${abilityId}`);
  };

  return (!pokemonDataLoading && pokemonData)
    ? (
      <div className={s.wrapper}>
        <div className={s.contentWrapper}>
          <div className={s.mainContent}>
            <div className={s.imageWrapper}>
              <img className={s.image} src={pokemonData.image} alt="" />
              <div className={s.title}>
                <span>{utils.capitalize(pokemonData.name)}</span>
              </div>
            </div>
            <div className={s.informationWrapper}>
              <div className={s.information}>
                <span
                  className={s.type}
                >
                  Type:
                  {' '}
                  {pokemonData.types.map((type: IPokemonType) => type.type.name).join(', ')}
                </span>
              </div>
              <div className={s.stats}>
                <span className={s.type}>Stats:</span>
                {pokemonData.stats.map((stat: IPokemonStat) => (
                  <div className={s.stat}>
                    <span>
                      {utils.capitalize(stat.stat.name)}
                      :
                      {' '}
                    </span>
                    <span>{stat.base_stat}</span>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className={s.abilities}>
            <div className={s.title}>
              Abilities:
            </div>
            {pokemonData.abilities.map((ability: IPokemonAbility) => (
              <button
                onClick={() => onAbilityClick(utils.extractIdFromUrl(ability.ability.url))}
                type="button"
              >
                {utils.capitalize(ability.ability.name)}
              </button>
            ))}
          </div>
          <ReturnButton />
        </div>
      </div>
    ) : (
      <FullPageSpinner />
    );
};
