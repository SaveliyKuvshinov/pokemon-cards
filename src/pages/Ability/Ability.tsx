import React, { useEffect } from 'react';
import { useParams } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { getAbilityData } from '../../actions/pokemonActions';
import { IRootState } from '../../interfaces';
import s from './ability.module.scss';
import { utils } from '../../utils';
import { ReturnButton } from '../../components/ReturnButton';
import { FullPageSpinner } from '../../components/Spinner';

export const Ability = () => {
  const { id }: {id: string} = useParams();
  const { abilityDataLoading, abilityData } = useSelector(
    (state: IRootState) => state.pokemonReducer,
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAbilityData(parseInt(id, 10)));
  }, [dispatch, id]);

  return !abilityDataLoading && abilityData.id === parseInt(id, 10)
    ? (
      <div className={s.wrapper}>
        <div className={s.textWrapper}>
          <div className={s.title}>{utils.capitalize(abilityData.name)}</div>
          <div className={s.description}>
            Short description:
            {abilityData.shortDescription}
          </div>
          <div className={s.description}>
            Full description:
            {abilityData.fullDescription}
          </div>
          <ReturnButton />
        </div>
      </div>
    )
    : (
      <FullPageSpinner />
    );
};
