import { paginationActions } from './types';
import { utils } from '../utils';

export const calculatePageCount = (pokemonCount: number) => ({
  type: paginationActions.SET_PAGE_COUNT,
  payload: Math.ceil(pokemonCount / utils.getCardsPerPage()),
});

export const setCurrentPage = (currentPage: number) => ({
  type: paginationActions.SET_CURRENT_PAGE,
  payload: currentPage,
});

export const setOffset = (offset: number) => ({
  type: paginationActions.SET_OFFSET,
  payload: offset,
});
