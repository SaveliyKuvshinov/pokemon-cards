import { Dispatch } from 'redux';
import { api } from '../api/v1';
import { pokemonActions } from './types';
import { calculatePageCount } from './paginationActions';
import { IAbilityData, IPokemon, IPokemonList } from '../interfaces';

const getPokemonListStarted = () => ({
  type: pokemonActions.GET_POKEMON_LIST_STARTED,
});

const getPokemonListSuccess = (data: IPokemonList) => ({
  type: pokemonActions.GET_POKEMON_LIST_SUCCESS,
  payload: data,
});

const getPokemonListFailure = (error: Error) => ({
  type: pokemonActions.GET_POKEMON_LIST_FAILURE,
  payload: error,
});

export const getPokemonList = (offset = 0) => (dispatch: Dispatch) => {
  dispatch(getPokemonListStarted());
  api.getPokemonList(offset)
    .then((data: IPokemonList | undefined) => {
      if (data) {
        dispatch(getPokemonListSuccess(data));
        dispatch(calculatePageCount(data.count));
      } else {
        throw new Error('Return data is undefined');
      }
    })
    .catch((error: Error) => {
      dispatch(getPokemonListFailure(error));
    });
};

const getPokemonDataStarted = () => ({
  type: pokemonActions.GET_POKEMON_DATA_STARTED,
});

const getPokemonDataSuccess = (data: IPokemon) => ({
  type: pokemonActions.GET_POKEMON_DATA_SUCCESS,
  payload: data,
});

const getPokemonDataFailure = (error: Error) => ({
  type: pokemonActions.GET_POKEMON_DATA_FAILURE,
  payload: error,
});

export const getPokemonData = (id: number) => (dispatch: Dispatch) => {
  dispatch(getPokemonDataStarted());
  api.getPokemonData(id)
    .then((data: IPokemon | undefined) => {
      if (data) {
        dispatch(getPokemonDataSuccess(data));
      } else {
        throw new Error('Return data is undefined');
      }
    })
    .catch((error: Error) => {
      dispatch(getPokemonDataFailure(error));
    });
};

const getAbilityDataStarted = () => ({
  type: pokemonActions.GET_ABILITY_DATA_STARTED,
});

const getAbilityDataSuccess = (data: IAbilityData) => ({
  type: pokemonActions.GET_ABILITY_DATA_SUCCESS,
  payload: data,
});

const getAbilityDataFailure = (error: Error) => ({
  type: pokemonActions.GET_ABILITY_DATA_FAILURE,
  payload: error,
});

export const getAbilityData = (id: number) => (dispatch: Dispatch) => {
  dispatch(getAbilityDataStarted());
  api.getAbilityData(id)
    .then((data: IAbilityData | undefined) => {
      if (data) {
        dispatch(getAbilityDataSuccess(data));
      } else {
        throw new Error('Return data is undefined');
      }
    })
    .catch((error: Error) => {
      dispatch(getAbilityDataFailure(error));
    });
};
