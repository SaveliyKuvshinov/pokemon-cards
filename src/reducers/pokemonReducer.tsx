import { pokemonActions } from '../actions/types';
import {
  IAbilityData, IAction, IPokemon, IPokemonState,
} from '../interfaces';

const initialState: IPokemonState = {
  abilityDataLoading: true,
  abilityDataErrors: [],
  abilityData: {} as IAbilityData,
  pokemonDataLoading: true,
  pokemonDataErrors: [],
  pokemonData: {} as IPokemon,
  pokemonListLoading: true,
  pokemonListErrors: [],
  pokemonList: {
    count: 0,
    results: [],
  },
};

export const pokemonReducer = (state = initialState, action: IAction) => {
  switch (action.type) {
    case pokemonActions.GET_POKEMON_LIST_STARTED:
      return {
        ...state,
        pokemonListLoading: true,
      };
    case pokemonActions.GET_POKEMON_LIST_SUCCESS:
      return {
        ...state,
        pokemonListLoading: false,
        pokemonList: action.payload,
      };
    case pokemonActions.GET_POKEMON_LIST_FAILURE:
      return {
        ...state,
        pokemonListErrors: [...state.pokemonListErrors, action.payload],
      };
    case pokemonActions.GET_POKEMON_DATA_STARTED:
      return {
        ...state,
        pokemonDataLoading: true,
      };
    case pokemonActions.GET_POKEMON_DATA_SUCCESS:
      return {
        ...state,
        pokemonDataLoading: false,
        pokemonData: action.payload,
      };
    case pokemonActions.GET_POKEMON_DATA_FAILURE:
      return {
        ...state,
        pokemonDataErrors: [...state.pokemonDataErrors, action.payload],
      };
    case pokemonActions.GET_ABILITY_DATA_STARTED:
      return {
        ...state,
        abilityDataLoading: true,
      };
    case pokemonActions.GET_ABILITY_DATA_SUCCESS:
      return {
        ...state,
        abilityDataLoading: false,
        abilityData: action.payload,
      };
    case pokemonActions.GET_ABILITY_DATA_FAILURE:
      return {
        ...state,
        abilityDataErrors: [...state.abilityDataErrors, action.payload],
      };
    default:
      return state;
  }
};
