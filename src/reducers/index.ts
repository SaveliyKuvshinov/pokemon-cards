import { combineReducers } from 'redux';
import { paginationReducer } from './paginationReducer';
import { pokemonReducer } from './pokemonReducer';

export const rootReducer = combineReducers({
  paginationReducer,
  pokemonReducer,
});
