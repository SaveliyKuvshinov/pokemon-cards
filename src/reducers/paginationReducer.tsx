import { paginationActions } from '../actions/types';
import { IAction, IPaginationState } from '../interfaces';

const initialState: IPaginationState = {
  pageCount: 0,
  currentPage: 1,
  offset: 0,
};

export const paginationReducer = (state = initialState, action: IAction) => {
  switch (action.type) {
    case paginationActions.SET_PAGE_COUNT:
      return {
        ...state,
        pageCount: action.payload,
      };
    case paginationActions.SET_CURRENT_PAGE:
      return {
        ...state,
        currentPage: action.payload,
      };
    case paginationActions.SET_OFFSET:
      return {
        ...state,
        offset: action.payload,
      };
    default:
      return state;
  }
};
