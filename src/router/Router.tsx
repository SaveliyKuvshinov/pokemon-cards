import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { CardList } from '../pages/CardList';
import { PokemonDetails } from '../pages/PokemonDetails';
import { Ability } from '../pages/Ability';

export const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" component={CardList} exact />
      <Route path="/details/:id" component={PokemonDetails} exact />
      <Route path="/ability/:id" component={Ability} exact />
    </Switch>
  </BrowserRouter>
);
