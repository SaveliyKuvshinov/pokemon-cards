import { utils } from '../utils';

const getPokemonList = async (offset: number) => {
  const response = await fetch(utils.getPokemonListUrl(utils.getCardsPerPage(), offset));

  if (!response.ok || !response.body) {
    throw new Error('response is not ok');
  }

  const data = await response.json();
  const results = data.results.map((result: any) => {
    const pokemonId = utils.extractIdFromUrl(result.url);

    return {
      id: pokemonId,
      url: result.url,
      name: result.name,
      image: utils.getPokemonImageUrl(pokemonId),
    };
  });

  return {
    results,
    count: data.count as number,
  };
};

const getPokemonData = async (id: number) => {
  const response = await fetch(utils.getPokemonDataUrl(id));

  if (!response.ok || !response.body) {
    throw new Error('response is not ok');
  }

  const data = await response.json();
  return {
    id,
    name: data.name,
    types: data.types,
    abilities: data.abilities,
    stats: data.stats,
    image: utils.getPokemonImageUrl(id),
  };
};

const getAbilityData = async (id: number) => {
  const response = await fetch(utils.getAbilityDataUrl(id));

  if (!response.ok || !response.body) {
    throw new Error('response is not ok');
  }

  const data = await response.json();
  const language = 'en';

  const descriptionEntry = data.effect_entries.find(
    (entry: any) => entry.language.name === language,
  );

  return {
    id,
    name: data.name,
    shortDescription: descriptionEntry.short_effect,
    fullDescription: descriptionEntry.effect,
  };
};

export const api = {
  getPokemonList,
  getPokemonData,
  getAbilityData,
};
